# CONFIGURATION VARIABLES
IMAGE_NAME = "bento/ubuntu-18.04"   # Image to use
K8S_NAME = "k8s-cluster"
MEM = 2048                          # Amount of RAM
CPU = 2                             # Number of processors (Minimum value of 2 otherwise it will not work)
MASTERS_NBR = 1                     # Number of masters node 
WORKERS_NBR = 1                     # Number of workers node
IP_BASE = "192.168.50."              # Main network


Vagrant.configure("2") do |config|
    config.ssh.insert_key = false

    # RAM and CPU config
    config.vm.provider "virtualbox" do |v|
        v.memory = MEM
        v.cpus = CPU
    end

    (1..MASTERS_NBR).each do |i|      
        config.vm.define "master-#{i}" do |master|
            master.vm.box = IMAGE_NAME
            master.vm.network "private_network", ip: "#{IP_BASE}#{i + 10}"
            master.vm.hostname = "master-#{i}"
            master.vm.provision "ansible" do |ansible|
                ansible.playbook = "roles/main.yml"
                #Generate Ansible Groups for inventory
                ansible.groups = {
                    "masters" => ["master-[1:#{MASTERS_NBR}]"],
                    "workers" => ["worker-[1:#{WORKERS_NBR}]"]
                }
                #Redefine defaults
                ansible.extra_vars = {
                    k8s_cluster_name:       K8S_NAME,                    
                    k8s_master_admin_user:  "vagrant",
                    k8s_master_admin_group: "vagrant",
                    k8s_master_apiserver_advertise_address: "#{IP_BASE}#{i + 10}",
                    k8s_master_node_name: "master-#{i}",
                    k8s_node_public_ip: "#{IP_BASE}#{i + 10}"
                }                
            end
        end
    end

    # Worker node config
    (1..WORKERS_NBR).each do |j|
        config.vm.define "worker-#{j}" do |worker|

            # Hostname and network config
            worker.vm.box = IMAGE_NAME
            worker.vm.network "private_network", ip: "#{IP_BASE}#{j + 10 + MASTERS_NBR}"
            worker.vm.hostname = "worker-#{j}"

            # Ansible role setting
            worker.vm.provision "ansible" do |ansible|

                # Ansbile role that will be launched
                ansible.playbook = "roles/main.yml"

                # Groups in Ansible inventory
                ansible.groups = {
                    "masters" => ["master-[1:#{MASTERS_NBR}]"],
                    "workers" => ["worker-[1:#{WORKERS_NBR}]"]
                }

                # Overload Ansible variables
                ansible.extra_vars = {
                    k8s_cluster_name:     K8S_NAME,
                    k8s_node_admin_user:  "vagrant",
                    k8s_node_admin_group: "vagrant",
                    k8s_node_public_ip: "#{IP_BASE}#{j + 10 + MASTERS_NBR}"
                }
            end
        end
    end
end